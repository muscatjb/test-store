# Refactoring Test

This simple API allows to fetch products, and add them to the user's cart.

A first set for features (v1) was released to production and still works fine.
An update as been made with new features (v2), but it isn't completed, with the developer not available anymore.

## To Fix

- Get Cart isn't working when the cart is empty
- Unknown performance issues on the Catalog when fetching a single product

- New fatures (v2) tests are red (Tests look to be good)

## To Improve

- Code duplication
- Code complexity
- Test coverage
- API design