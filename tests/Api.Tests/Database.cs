﻿using System;
using Data;

namespace Api.Tests
{
    internal class Database
    {
        public static Database Instance { get; }

        static Database()
        {
            Instance = new Database();
        }

        private Database()
        {
        }

        public Database WithProduct(out ProductEntity product, string? name = null, int price = 10)
        {
            var uid = Guid.NewGuid();
            return Add(product = new ProductEntity
            {
                Id = uid,
                Price = price,
                Name = name ?? $"Product {uid:N}",
            });
        }

        private Database Add<T>(T entity)
            where T : notnull
        {
            using (var context = TestSetUpFixture.StoreContextFactory())
            {
                context.Add(entity);
                context.SaveChanges();
            }

            return Instance;
        }
    }
}
