using System;
using System.Threading.Tasks;
using NUnit.Framework;
using Api.Models;
using Api.Models.Users;

namespace Api.Tests
{
    public class CartsTests
    {
        public class V1
        {
            [Test]
            public async Task With_Two_Different_Product_Should_Have_Five_Percent_Discount()
            {
                // Arrange
                Database.Instance
                    .WithProduct(out var productA, price: 50)
                    .WithProduct(out var productB, price: 50);

                var token = await new RequestBuilder()
                    .UserLogin()
                    .Execute<TokenModel>();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productB.Id)
                    .Send();

                // Act
                var cart = await new RequestBuilder()
                    .WithToken(token)
                    .GetUserCart()
                    .Execute<CartModel>();

                // Assert
                Assert.That(cart, Is.Not.Null);
                Assert.That(cart.P_T, Is.EqualTo(100));
                Assert.That(cart.P_F, Is.EqualTo(95));
                Assert.That(cart.P_R, Is.EqualTo(5));
            }

            [Test]
            public async Task With_Three_Different_Product_Should_Have_Ten_Percent_Discount()
            {
                // Arrange
                Database.Instance
                    .WithProduct(out var productA, price: 30)
                    .WithProduct(out var productB, price: 30)
                    .WithProduct(out var productC, price: 40);

                var token = await new RequestBuilder()
                    .UserLogin()
                    .Execute<TokenModel>();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productB.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productC.Id)
                    .Send();

                // Act
                var cart = await new RequestBuilder()
                    .WithToken(token)
                    .GetUserCart()
                    .Execute<CartModel>();

                // Assert
                Assert.That(cart, Is.Not.Null);
                Assert.That(cart.P_T, Is.EqualTo(100));
                Assert.That(cart.P_F, Is.EqualTo(90));
                Assert.That(cart.P_R, Is.EqualTo(10));

            }

            [Test]
            public async Task With_Four_Different_Product_Should_Have_Twelve_Percent_Discount()
            {
                // Arrange
                Database.Instance
                    .WithProduct(out var productA, price: 25)
                    .WithProduct(out var productB, price: 25)
                    .WithProduct(out var productC, price: 25)
                    .WithProduct(out var productD, price: 25);

                var token = await new RequestBuilder()
                    .UserLogin()
                    .Execute<TokenModel>();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productB.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productC.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productD.Id)
                    .Send();

                // Act
                var cart = await new RequestBuilder()
                    .WithToken(token)
                    .GetUserCart()
                    .Execute<CartModel>();

                // Assert
                Assert.That(cart, Is.Not.Null);
                Assert.That(cart.P_T, Is.EqualTo(100));
                Assert.That(cart.P_F, Is.EqualTo(88));
                Assert.That(cart.P_R, Is.EqualTo(12));
            }
        }

        public class V2
        {
            [Test]
            public async Task With_Two_Similar_Product_And_Another_Different_Should_Have_Fifteen_Percent_Discount()
            {
                // Arrange
                Database.Instance
                    .WithProduct(out var productA, price: 25)
                    .WithProduct(out var productB, price: 50);

                var token = await new RequestBuilder()
                    .UserLogin()
                    .Execute<TokenModel>();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productB.Id)
                    .Send();

                // Act
                var cart = await new RequestBuilder()
                    .WithToken(token)
                    .GetUserCart()
                    .Execute<CartModel>();

                // Assert
                Assert.That(cart, Is.Not.Null);
                Assert.That(cart.P_T, Is.EqualTo(100));
                Assert.That(cart.P_F, Is.EqualTo(85));
                Assert.That(cart.P_R, Is.EqualTo(15));
            }

            [Test]
            public async Task With_Two_Similar_Product_And_Two_Other_Similar_Product_Should_Have_Twenty_Percent_Discount()
            {
                // Arrange
                Database.Instance
                    .WithProduct(out var productA, price: 25)
                    .WithProduct(out var productB, price: 25);

                var token = await new RequestBuilder()
                    .UserLogin()
                    .Execute<TokenModel>();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productA.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productB.Id)
                    .Send();

                await new RequestBuilder()
                    .WithToken(token)
                    .AddProductToUserCart(productB.Id)
                    .Send();

                // Act
                var cart = await new RequestBuilder()
                    .WithToken(token)
                    .GetUserCart()
                    .Execute<CartModel>();

                // Assert
                Assert.That(cart, Is.Not.Null);
                Assert.That(cart.P_T, Is.EqualTo(100));
                Assert.That(cart.P_F, Is.EqualTo(80));
                Assert.That(cart.P_R, Is.EqualTo(20));
            }
        }
    }
}