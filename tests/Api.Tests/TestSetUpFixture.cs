﻿using Data;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NUnit.Framework;
using System;
using System.Net.Http;

namespace Api.Tests
{
    [SetUpFixture]
    public class TestSetUpFixture
    {
        public static string TestRunIdentifier = Guid.NewGuid().ToString("N");

        public static HttpClient Client { get; private set; }

        public static Func<StoreContext> StoreContextFactory { get; private set; }

        [OneTimeSetUp]
        public static void RunBeforeTheFirstTest()
        {
            Client = new WebApplicationFactory<Program>()
                .WithWebHostBuilder(builder => builder.ConfigureTestServices(OverridesServices))
                .CreateClient();

            StoreContextFactory = () =>
            {
                var context = new StoreContext(TestRunIdentifier);
                context.Database.EnsureCreated();
                return context;
            };
        }

        private static void OverridesServices(IServiceCollection services)
        {
            services
                .RemoveAll<StoreContext>()
                .AddTransient(provider => StoreContextFactory());
        }

        [OneTimeTearDown]
        public void RunAfterTheLastTest()
        {
            Client.Dispose();
        }
    }
}
