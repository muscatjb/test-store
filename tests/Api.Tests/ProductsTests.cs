using Api.Models.Products;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Api.Tests
{
    public class ProductsTests
    {
        [Test]
        public async Task Get_Returns_All_Products()
        {
            await using var application = new WebApplicationFactory<Program>();
            using var client = application.CreateClient();

            var response = await client.GetAsync("/products");
            var json = await response.Content.ReadAsStringAsync();
            var products = JsonConvert.DeserializeObject<ProductModel[]>(json);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(products, Has.Length.EqualTo(5));
        }

        [TestCase("d5f562e2b9b140a2878669b12950549e", "Shoes")]
        [TestCase("d62f85f80ea34891846b40a1a899a749", "Shirt")]
        public async Task GetById_Return_Given_Product_By_Its_Id(string uid, string name)
        {
            await using var application = new WebApplicationFactory<Program>();
            using var client = application.CreateClient();

            var response = await client.GetAsync($"/products/{uid}");
            var json = await response.Content.ReadAsStringAsync();
            var product = JsonConvert.DeserializeObject<ProductModel>(json);

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(product.Name, Is.EqualTo(name));
        }
    }
}