﻿using Api.Models.Users;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Api.Tests
{
    internal class RequestBuilder
    {
        private  Guid _user;

        private readonly HttpClient _client;
        public readonly HttpRequestMessage Request;

        public RequestBuilder()
        {
            Request = new HttpRequestMessage();
            _client = TestSetUpFixture.Client;
        }

        public RequestBuilder GetProduct(Guid uid)
        {
            Request.Method = HttpMethod.Get;
            Request.RequestUri = new Uri($"/products/{uid}", UriKind.Relative);
            return this;
        }

        public RequestBuilder WithToken(TokenModel token)
        {
            Request.Headers.Add(HeaderNames.Authorization, $"{token.Type} {token.Value}");
            return this;
        }

        public RequestBuilder GetProducts()
        {
            Request.Method = HttpMethod.Get;
            Request.RequestUri = new Uri($"/products", UriKind.Relative);
            return this;
        }

        public RequestBuilder GetUserCart()
        {
            Request.Method = HttpMethod.Get;
            Request.RequestUri = new Uri($"/carts?user={_user}", UriKind.Relative);
            return this;
        }

        public RequestBuilder AddProductToUserCart(Guid uid)
        {
            Request.Method = HttpMethod.Post;
            Request.RequestUri = new Uri($"/carts/{uid}?user={_user}", UriKind.Relative);
            Request.Content = new StringContent(String.Empty);
            return this;
        }

        public RequestBuilder UserLogin()
        {
            Request.Method = HttpMethod.Post;
            Request.RequestUri = new Uri($"/users/login", UriKind.Relative);
            Request.Content = new StringContent(String.Empty);
            return this;
        }

        public async Task<HttpResponseMessage> Send()
        {
            return await _client.SendAsync(Request);
        }

        public async Task<T> Execute<T>()
        {
            var response = await _client.SendAsync(Request);
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
