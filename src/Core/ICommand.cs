﻿namespace Core
{
    public interface ICommand
    {
        bool IsValid { get; }

        List<ValidationError> ValidationErrors { get; }
    }
}