﻿namespace Core
{
    public class CommandResult
    {
        public bool IsSuccess { get; }

        public Exception? Exception { get; }

        public CommandResult()
        {

        }

        public CommandResult(Exception exception)
        {
            Exception = exception;
        }
    }
}