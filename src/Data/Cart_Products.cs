﻿using System;

namespace Data
{
    public class Cart_Products
    {
        public Guid Id { get; set; }

        public Guid Id_Cart { get; set; }

        public Guid Id_Product { get; set; }

        public virtual CartEntity Cart { get; set; }

        public virtual ProductEntity Product { get; set; }
    }
}
