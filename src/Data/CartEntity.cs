﻿using System;

namespace Data
{
    public class CartEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string User { get; set; }

        public virtual ICollection<Cart_Products> Products { get; set; } = new List<Cart_Products>();
    }
}
