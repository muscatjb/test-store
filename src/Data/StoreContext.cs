﻿using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class StoreContext : DbContext
    {
        private readonly string _identifier;

        public DbSet<ProductEntity> Products { get; set; }

        public DbSet<CartEntity> Carts { get; set; }

        public StoreContext(string identifier)
        {
            _identifier = identifier;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options
                .UseInMemoryDatabase(_identifier);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CartEntity>()
                .HasMany(cart => cart.Products);

            modelBuilder.Entity<Cart_Products>()
                .HasOne(cartProduct => cartProduct.Cart)
                .WithMany()
                .HasForeignKey(cartProduct => cartProduct.Id_Cart);

            modelBuilder.Entity<Cart_Products>()
                .HasOne(cartProduct => cartProduct.Product)
                .WithMany()
                .HasForeignKey(cartProduct => cartProduct.Id_Product);

            modelBuilder.Entity<ProductEntity>()
                .HasData(
                    new ProductEntity { Id = new Guid("d5f562e2b9b140a2878669b12950549e"), Name = "Shoes", Price = 99.99 },
                    new ProductEntity { Id = new Guid("9e5a4b7bc58f4cb98a528fb804d1048d"), Name = "Pant", Price = 129.59 },
                    new ProductEntity { Id = new Guid("2d44c78e587b4d76a034f13c8f24db29"), Name = "Jacket", Price = 157.95 },
                    new ProductEntity { Id = new Guid("70b011685f5c4b4cb74a1f306d883aae"), Name = "Cap", Price = 34.99 },
                    new ProductEntity { Id = new Guid("d62f85f80ea34891846b40a1a899a749"), Name = "Shirt", Price = 79.95 }
                );
        }
    }
}