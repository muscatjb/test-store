﻿using System;

namespace Data
{
    public class ProductEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string? Name { get; set; }

        public double Price { get; set; }
    }
}
