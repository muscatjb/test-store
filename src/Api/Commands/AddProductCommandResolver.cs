﻿using Core;
using Data;
using Microsoft.EntityFrameworkCore;

namespace Api.Commands
{
    public class AddProductCommandResolver
    {
        private readonly StoreContext _context;
        private readonly ILogger<AddProductCommandResolver> _logger;

        public AddProductCommandResolver(StoreContext context, ILogger<AddProductCommandResolver> logger)
        {
            _context = context;
            _logger = logger;
        }

        public CommandResult Resolve(AddProductCommand command)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "{0} failed with exception {1}.", nameof(AddProductCommand), exception.GetType().Name);
                return new CommandResult(exception);
            }
        }
    }
}
