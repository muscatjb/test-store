﻿using Api.Controllers;
using Core;
using Microsoft.AspNetCore.Mvc;

namespace Api.Commands
{
    public class AddProductCommand : ICommand
    {
        public string User { get; }

        public Guid ProductId { get; }

        public bool IsValid
        {
            get
            {
                if (ValidationErrors == null)
                    Validate();

                return !ValidationErrors.Any();
            }
        }

        public List<ValidationError> ValidationErrors { get; private set; }

        public AddProductCommand(string user, Guid productId)
        {
            User = user;
            ProductId = productId;

            ValidationErrors = new List<ValidationError>();
        }

        private void Validate()
        {
            ValidationErrors = new List<ValidationError>();

            if (string.IsNullOrEmpty(User))
                ValidationErrors.Add(new ValidationError("User", "The user can't be null or empty."));

            if (ProductId == Guid.Empty)
                ValidationErrors.Add(new ValidationError("ProductId", "The product can't be null or empty."));
        }
    }
}
