using System;
using Api.Commands;
using Api.Extensions;
using Api.Models;
using Api.Models.Products;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
    [Route("[controller]"), Authorize, ApiController]
    public class CartsController : ControllerBase
    {
        private readonly StoreContext _context;
        private readonly AddProductCommandResolver _resolver;

        public CartsController(StoreContext context, AddProductCommandResolver resolver)
        {
            _context = context ?? throw new ArgumentNullException(nameof(StoreContext));
            _resolver = resolver ?? throw new ArgumentNullException(nameof(AddProductCommandResolver));
        }

        [HttpGet]
        public CartModel Get()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "app:uid")?.Value;

            var cart = _context.Carts
                .Include(a => a.Products)
                .ThenInclude(a => a.Product)
                .First(a => a.User == user);

            var i = cart.Products.Sum(a => a.Product.Price);

            #region v1
            if (cart.Products.Select(a => a.Product.Id).Distinct().Count() == 2)
                i = i - (i / 100 * 5);

            if (cart.Products.Select(a => a.Product.Id).Distinct().Count() == 3)
                i = i - i / 100 * 10;

            if (cart.Products.Select(a => a.Product.Id).Distinct().Count() == 4)
                i = i - i / 100 * 12;
            #endregion

            #region v2
            if (cart.Products.Distinct().Count() == 2 && cart.Products.Count() == 3)
                i -= i * 0.15;

            if (cart.Products.Distinct().Count() == 2 && cart.Products.Count() == 4)
                i -= i * 0.2;
            #endregion

            return new CartModel
            {
                User = user,
                Products = cart.Products.Select(a => new ProductModel
                {
                    Id = a.Product.Id,
                    Name = a.Product.Name,
                    Price = a.Product.Price,
                }),
                P_R = Math.Abs(i - cart.Products.Sum(a => a.Product.Price)),
                P_T = cart.Products.Sum(a => a.Product.Price),
                P_F = i,
            };
        }

        [HttpPost("{id:guid}")]
        public CartModel AddProduct(Guid id)
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "app:uid")?.Value;
            var cart = _context.Carts.Include(a => a.Products).FirstOrDefault(a => a.User == user);

            if (cart == null)
            {
                cart = _context.Add(new CartEntity
                {
                    User = user,
                }).Entity;
            }

            cart.Products.Add(new Cart_Products { Cart = cart, Id_Product = id });

            _context.SaveChanges();

            return new CartModel
            {
                User = user,
            };
        }

        [HttpPost("add-product-v2")]
        public IActionResult AddProduct(AddProductModel model)
        {
            var command = new AddProductCommand(model.User, model.ProductId);

            if (!command.IsValid)
                return BadRequest(command.ValidationErrors);

            return _resolver
                .Resolve(command)
                .ToActionResult();
        }
    }
}
