using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Api.Models.Users;
using Api.Options;
using Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Api.Controllers
{
    [Route("[controller]"), AllowAnonymous, ApiController]
    public class UsersController : ControllerBase
    {
        private readonly StoreContext _context;
        private readonly AuthenticationOptions _options;

        public UsersController(StoreContext context, IOptions<AuthenticationOptions> options)
        {
            _context = context ?? throw new ArgumentNullException(nameof(StoreContext));
            _options = options?.Value ?? throw new ArgumentNullException(nameof(AuthenticationOptions));
        }

        [HttpPost("login")]
        public IActionResult Login()
        {
            var claims = new[] { new Claim("app:uid", Guid.NewGuid().ToString("N")) };

            var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.Jwt.Secret));
            var credentials = new SigningCredentials(secret, SecurityAlgorithms.HmacSha256);
            var options = new JwtSecurityToken(issuer: _options.Jwt.Issuer, audience: _options.Jwt.Audience, claims: claims, expires: DateTime.Now.AddMinutes(6), signingCredentials: credentials);
            var token = new JwtSecurityTokenHandler().WriteToken(options);

            return Ok(new TokenModel { Type = JwtBearerDefaults.AuthenticationScheme, Value = token });
        }

        [HttpPost("register")]
        public IActionResult Register()
        {
            throw new NotImplementedException();
        }
    }
}
