using Microsoft.AspNetCore.Mvc;
using Api.Models.Products;
using Data;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers;

[Route("[controller]"), AllowAnonymous, ApiController]
public class ProductsController : ControllerBase
{
    private readonly StoreContext _context;

    public ProductsController(StoreContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(StoreContext));
    }

    [HttpGet("{id:guid}")]
    public ProductModel GetById(Guid id)
    {
        return Get().Where(a => a.Id == id).Single();
    }

    [HttpGet]
    public IEnumerable<ProductModel> Get()
    {
        return _context.Products
            .Select(a => new ProductModel
            {
                Id = a.Id,
                Name = a.Name,
                Price = a.Price,
            })
            .ToList();
    }
}
