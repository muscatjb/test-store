﻿using Core;
using Microsoft.AspNetCore.Mvc;

namespace Api.Extensions
{
    public static class CommandResultExtensions
    {
        public static IActionResult ToActionResult(this CommandResult commandResult)
        {
            if (commandResult.Exception != null)
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);

            return new OkResult();
        }
    }
}
