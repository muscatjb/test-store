﻿using Api.Commands;
using Api.Options;
using Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Text;

namespace Api
{
    public class Startup
    {
        public readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Startup ConfigureServices(IServiceCollection services)
        {
            var identifier = Guid.NewGuid().ToString("N");

            var authenticationSection = _configuration.GetSection(AuthenticationOptions.Authentication);
            var authenticationOptions = authenticationSection.Get<AuthenticationOptions>();

            services.Configure<AuthenticationOptions>(authenticationSection);

            services.AddControllers();
            services.AddEndpointsApiExplorer();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "WebAPI",
                    Description = "Product WebAPI"
                });

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Description = "Bearer Authentication with JWT Token",
                    Type = SecuritySchemeType.Http
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List < string > ()
                    }
                });
            });

            services.AddScoped<AddProductCommandResolver>();

            services.AddScoped(provider =>
            {
                var context = new StoreContext(identifier);
                context.Database.EnsureCreated();
                return context;
            });

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = authenticationOptions.Jwt.Issuer,
                        ValidAudience = authenticationOptions.Jwt.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationOptions.Jwt.Secret))
                    };
                });

            return this;
        }

        public WebApplication Configure(WebApplication application, IWebHostEnvironment environment)
        {
            if (environment.IsDevelopment())
            {
                application.UseSwagger();
                application.UseSwaggerUI();
            }
            else
            {
                application.UseHttpsRedirection();
            }

            application.UseAuthentication();
            application.UseAuthorization();
            application.MapControllers();

            return application;
        }
    }
}