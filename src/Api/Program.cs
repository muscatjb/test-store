using System;

namespace Api
{
    public class Program
    {
        public static void Main(params string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var startup = new Startup(builder.Configuration);

            startup
                .ConfigureServices(builder.Services)
                .Configure(builder.Build(), builder.Environment)
                .Run();
        }
    }
}

