﻿namespace Api.Options
{
    public class AuthenticationOptions
    {
        public const string Authentication = nameof(Authentication);

        public JwtAuthenticationOptions Jwt { get; set; }

        public class JwtAuthenticationOptions
        {
            public string Secret { get; set; }

            public string Issuer { get; set; }

            public string Audience { get; set; }
        }
    }
}
