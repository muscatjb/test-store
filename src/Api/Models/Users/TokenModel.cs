﻿namespace Api.Models.Users
{
    public class TokenModel
    {
        public string Type { get; set; }

        public string Value { get; set; }
    }
}
