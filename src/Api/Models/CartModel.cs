﻿using Api.Models.Products;

namespace Api.Models
{
    public class CartModel
    {
        public string User { get; set; }

        public string? DiscountName { get; set; }

        public double P_T { get; set; }

        public double P_R { get; set; }

        public double P_F { get; set; }

        public IEnumerable<ProductModel>? Products { get; set; }
    }
}
