﻿namespace Api.Models.Products
{
    public class ProductModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }
    }
}
