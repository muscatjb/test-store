﻿namespace Api.Models
{
    public class AddProductModel
    {
        public string User { get; internal set; }

        public Guid ProductId { get; internal set; }
    }
}
